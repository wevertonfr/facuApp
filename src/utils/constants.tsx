export const cidades = [
  {
    id: 1239133443850240,
    title: "sao paulooo"
  },
  {
    id: 231383326785536,
    title: "rio de janeiro"
  },
  {
    id: 706475915739136,
    title: "salvador"
  },
  {
    id: 4125970632015872,
    title: "curitba"
  },
  {
    id: 1763778655944704,
    title: "goiania"
  },
  {
    id: 5157660466872320,
    title: "forteleza"
  }
];

export const sexos = [
  {
    text: "Masculino"
  },
  {
    text: "Feminino"
  }
];

export const bairros = [
  {
    id: 6329383298007040,
    title: "bueno"
  },
  {
    id: 3058415700017152,
    title: "marista"
  },
  {
    id: 2137622151430144,
    title: "setor central"
  },
  {
    id: 612198661488640,
    title: "vila morais"
  },
  {
    id: 1047437850443776,
    title: "aldeia do vale"
  },
  {
    id: 311133600743424,
    title: "setor aeroporto"
  },
  {
    id: 1507104552648704,
    title: "universitario"
  },
  {
    id: 3250187682185216,
    title: "madre germana"
  },
  {
    id: 455632096329728,
    title: "itaquera"
  },
  {
    id: 4281376928956416,
    title: "vila multirao"
  },
  {
    id: 1282643947683840,
    title: "jardim liberdade"
  },
  {
    id: 1017401032310784,
    title: "palmares"
  },
  {
    id: 7019391130533888,
    title: "jardim america"
  },
  {
    id: 3641861711331328,
    title: "bairro da vitoria"
  },
  {
    id: 5152519823032320,
    title: "buriti sereno"
  },
  {
    id: 1931839988039680,
    title: "garavelo"
  },
  {
    id: 8421938980454400,
    title: "cidade satelite de sao luiz"
  }
];